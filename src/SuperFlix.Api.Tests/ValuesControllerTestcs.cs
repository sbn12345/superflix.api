﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using SuperFlix.Api.Controllers;

namespace SuperFlix.Api.Tests
{
    [TestFixture]
    public class ValuesControllerTestcs
    {
        [Test]
        public void get_all_return_value1_and_value2()
        {
            var controller = new ValuesController();

            var values = controller.Get();

            Assert.AreEqual(new [] { "value1", "value2" }, values);

        }
    }
}
